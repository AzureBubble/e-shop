# 仿京东网上商城

#### 介绍

自主使用微信小程序实现的仿京东网上商城案例（包括前端和后台），数据库利用了微信小程序的云数据库

#### 软件架构

软件架构说明

#### 数据库表设计

本系统数据库主要用了微信小程序自带的云数据库实现。

云数据库的优点：该数据库开发者可以使用云开发开发微信小程序、小游戏，无需搭建服务器，即可使用云端能力。

云开发为开发者提供完整的云端支持，弱化后端和运维概念，无需搭建服务器，使用平台提供的 API 进行核心业务开发，

即可实现快速上线和迭代，同时这一能力，同开发者已经使用的云服务相互兼容，并不互斥。

![数据库表关系图](https://images.gitee.com/uploads/images/2021/0412/165327_0afa6043_7644000.jpeg "数据库表设计.jpg")

​																							                    
                                                数据库表关系图

##### 数据表 ：

![](README.assets/admin表.jpg)

​																							admin表

![](README.assets/goodsList表.jpg)

​																									goodsList表

![](README.assets/cartList表.jpg)

​																									购物车表

![](README.assets/addressList表.jpg)

​																								addressList表

![](README.assets/classifyList表.jpg)

​																								classifyList表

![](README.assets/collectList表.jpg)

​																								coolectList表

![](README.assets/orderList表.jpg)

​																							orderList表

![](README.assets/shippedList表.jpg)

​																									shippedList表



#### 前端运行效果

![](README.assets/首页.jpg)

​																											首页效果图



![](README.assets/分类.jpg)

​																											分类效果图

![ ](README.assets/购物车.jpg)

​																										购物车效果图

![](README.assets/购物车编辑.jpg)

​																									购物车编辑效果图

![](README.assets/我的.jpg)

​																										我的效果图

![](README.assets/搜索.jpg)

​																											搜索效果图

![](README.assets/商品详情.jpg)

​																										商品详情效果图

![](README.assets/我的订单.jpg)

​																										我的订单效果图

![](README.assets/我的收藏.jpg)

​																										我的收藏效果图

![](README.assets/我的收藏编辑.jpg)

​																										我的收藏编辑效果图

![](README.assets/物流查询.jpg)

​                 																					订单物流查询效果图

![](README.assets/收货地址管理.jpg)

​																									收货地址管理效果图

![](README.assets/收货地址修改.jpg)

​																										收货地址修改效果图

![](README.assets/新增收货地址.jpg)

​																									新增收货地址效果图

#### 后台管理运行效果

##### 后台登录界面

默认账号：admin  密码：123456

![](README.assets/后台登录页面.jpg)

​																										后台登录页面效果图

![](README.assets/商城后台.jpg)

​																										商城后台管理效果图

##### 管理员修改密码

![](README.assets/管理员密码修改-1607752132578.jpg)

​																										修改密码效果图

##### 订单管理页面

![](README.assets/订单管理.jpg)

​																										订单管理效果图

![](README.assets/已发货订单.jpg)

​																									已发货订单效果图

##### 分类管理

![](README.assets/分类管理.jpg)

​																										分类管理效果图

![](README.assets/分类编辑.jpg)

​																									分类编辑效果图

![](README.assets/修改分类.jpg)

​																									修改分类效果图

![](README.assets/新增分类.jpg)

​																									新增分类效果图

##### 商品管理

![](README.assets/商品管理.jpg)

​																										商品管理效果图

![](README.assets/商品编辑-1607752360254.jpg)

​																									商品编辑效果图

![](README.assets/修改商品.jpg)

​																										修改商品效果图

![](README.assets/新增商品.jpg)

​																										新增商品效果图

